package br.com.itau;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Agenda {
    List<Contato> contatos;
    private int tamanhoAgenda;

    public Agenda(int tamanhoAgenda) {
        this.tamanhoAgenda = tamanhoAgenda;
        this.contatos = new ArrayList<Contato>();
    }
    public Agenda(){

    }

    public int getTamanhoAgenda() {
        return tamanhoAgenda;
    }

    public boolean removePorEmail(String email) {

        Contato contato = contatos.stream().
                filter(p -> p.getEmail().equals(email)).
                findAny().
                orElse(null);
        if(contato != null) return true;
        else return false;

//        if (email.contains("@")) {
//            for (Contato contato : this.contatos) {
//                if (contato.email.equals(email)) {
//                    this.contatos.remove(contato);
//                    return true;
//                }
//            }
//        }
//        return false;
    }

    public boolean removePorNumero(int numero) {

        Contato contato = contatos.stream().
                filter(p -> p.getNumero()==numero).
                findAny().
                orElse(null);
        if(contato != null) return true;
        else return false;
//
//        for (Contato contato : this.contatos) {
//            if (contato.numero == numero) {
//                this.contatos.remove(contato);
//                return true;
//            }
//        }
//        return false;
    }

    public void preencheAgendaTeste() {
        Contato contato;

        for (int i = 1; i < this.tamanhoAgenda + 1; i++) {
            contato = new Contato();
            contato.setEmail("pessoa" + i + "@email.com");
            contato.setNome("pessoa" + i);
            contato.setNumero(i);
            this.contatos.add(contato);
            //System.out.println("Contato (" + i + ") - Nome: " + contato.getNome() + " |email: " + contato.getEmail() + " |numero: " + contato.getNumero());
        }

    }

    public Contato getContato(String email){

        return contatos.stream().
                filter(p -> p.getEmail().equals(email)).
                findAny().
                orElse(null);

//        for (Contato contato : contatos) {
//            if (contato.getEmail().toUpperCase().equals(email.toUpperCase())){
//                return contato;
//            }
//        }
//        return null;
    }
    public Contato getContato(int numero){

        return contatos.stream().
                filter(p -> p.getNumero()==numero).
                findAny().
                orElse(null);
//
//        for (Contato contato : contatos) {
//            if (contato.getNumero() == numero) {
//                return contato;
//            }
//        }
//        return null;
    }

}
