package br.com.itau;

import java.util.Scanner;

public class Impressora {

    public void menu(Agenda agenda) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Defina a opção:");
        System.out.print("1 - Exclusao | 2 - Consulta | 3 - Sair do programa : ");
        int input = scanner.nextInt();
        if (input == 1) subMenuExclusao(agenda);
        else if (input == 2) subMenuConsulta(agenda);
        else if (input == 3) System.out.println("Sistema encerrado, obrigado e até logo!");
        else {
            System.out.println("Opção inválida");
            menu(agenda);
        }
    }

    public void subMenuExclusao(Agenda agenda) {
        Scanner scanner = new Scanner(System.in);
        String input = "";

        while (!input.equals("N") || !input.equals("N") || !input.equals("V")) {
            System.out.print("Defina a opção de exclusão: E - Email | N - Numero | V - Volta Menu: ");
            input = scanner.nextLine().toUpperCase();

            if (input.equals("E")) {
                if (agenda.removePorEmail(defineExcluirContatoPorEmail())) {
                    System.out.println("Registro Excluido");
                } else {
                    System.out.println("Registro não encontrado");
                }
            } else if (input.equals("N")) {
                if (agenda.removePorNumero(defineExcluirContatoPorNumero())) {
                    System.out.println("Registro Excluido");
                } else {
                    System.out.println("Registro não encontrado");
                }
            } else if (input.equals("V")) menu(agenda);
            else System.out.println("Opção Inválida, tente novamente");
        }

        input = "";
        while (!input.equals("S") || !input.equals("N")) {
            System.out.print("Deseja fazer nova Exclusão? S - Sim | V - Voltar Menu : ");
            input = scanner.nextLine().toUpperCase();
            if (input.equals("V")) menu(agenda);
            else if (input.equals("S")) subMenuExclusao(agenda);
            else System.out.println("Opção Inválida, tente novamente");
        }
    }

    public void subMenuConsulta(Agenda agenda) {
        Contato contato = new Contato();
        Scanner scanner = new Scanner(System.in);
        String input = "";

        while (!input.equals("E") || !input.equals("N") || !input.equals("V") || !input.equals("T")) {
            System.out.print("Defina a opção de consulta: E - Email | N - Numero | T - Todos | V - Volta Menu: ");
            input = scanner.nextLine().toUpperCase();

            if (input.equals("E")) {
                contato = agenda.getContato(definePesquisaContatoPorEmail());
                if (contato != null) {
                    imprimeContato(contato);
                } else {
                    System.out.println("Registro não encontrado");
                }
            } else if (input.equals("N")) {
                contato = agenda.getContato(definePesquisaContatoPorNumero());
                if (contato != null) {
                    imprimeContato(contato);
                } else {
                    System.out.println("Registro não encontrado");
                }
            } else if (input.equals("T")) {
                imprimeAgenda(agenda);
            }else if (input.equals("V")) menu(agenda);
            else System.out.println("Opção Inválida, tente novamente");
        }

        input = "";
        while (!input.equals("S") || !input.equals("N")) {
            System.out.print("Deseja fazer nova Consulta? S - Sim | V - Voltar Menu : ");
            input = scanner.nextLine().toUpperCase();
            if (input.equals("V")) menu(agenda);
            else if (input.equals("S")) subMenuConsulta(agenda);
            else System.out.println("Opção Inválida, tente novamente");
        }
    }

    public int defineTamanhoAgenda() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Por favor, defina o tamanho da sua Agenda: ");
        return scanner.nextInt();
    }

    public String defineExcluirContatoPorEmail() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite o email do contato que seja excluir: ");
        return scanner.nextLine();
    }

    public int defineExcluirContatoPorNumero() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite o numero do contato que seja excluir: ");
        return scanner.nextInt();
    }

    public void imprimeAgenda(Agenda agenda) {
        for (Contato contato : agenda.contatos) {
            System.out.println("Contato numero (" + contato.numero + ") - Nome: " + contato.nome + " |email: "
                    + contato.email);
        }

    }

    public boolean imprimeContatoPorEmail(String email, Agenda agenda) {
        for (Contato contato : agenda.contatos) {
            if (contato.email.equals(email)) {
                System.out.println("Contato numero (" + contato.numero + ") - Nome: " + contato.nome + " |email: "
                        + contato.email);
                return true;
            }
        }
        return false;
    }

    public String definePesquisaContatoPorEmail() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite o email do contato que seja consultar: ");
        return scanner.nextLine();
    }

    public int definePesquisaContatoPorNumero() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite o numero do contato que seja consultar: ");
        return scanner.nextInt();
    }

    public void imprimeContato(Contato contato) {
        System.out.println("Contato numero (" + contato.numero + ") - Nome: " + contato.nome + " |email: "
                + contato.email);
    }


}
